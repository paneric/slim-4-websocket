<?php

declare(strict_types=1);

namespace App;

use Psr\Http\Message\ServerRequestInterface as Request;
use ElephantIO\Client;

class MessageService
{
    private $client;
    
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function emit(Request $request): string
    {
        if ($request->getMethod() === 'POST') {

            $attributes = $request->getParsedBody();

            $this->client->initialize();
            $this->client->emit('chat message', ['body' => $attributes['msg']]);
            $this->client->close();
    
            return 'success';
        }

        return 'failure';
    }
}
