<?php

declare(strict_types=1);

namespace App;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Views\Twig;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class MessageController
{
    private $service;

    public function __construct(MessageService $service)
    {
        $this->service = $service;
    }

    public function emit(Request $request, Response $response, Twig $twig): Response
    {   
        if ($request->hasHeader('X-Requested-With')) {
            $response->getBody()->write(
                $this->service->emit($request)
            );
            return $response;
        }

        try {
            return $twig->render(
                $response,
                'chat.html.twig'
            );
        } catch (LoaderError $e) {
            echo $e->getMessage();
        } catch (RuntimeError $e) {
            echo $e->getMessage();
        } catch (SyntaxError $e) {
            echo $e->getMessage();
        }

        return $response;
    }
}
