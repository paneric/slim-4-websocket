<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use DI\ContainerBuilder;
use DI\Container;
use DI\Bridge\Slim\Bridge;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use App\MessageController;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;

require __DIR__ . '/../vendor/autoload.php';

$builder = new ContainerBuilder();// integration of php-di
$builder->useAutowiring(true);// integration of php-di
$builder->addDefinitions([// integration of php-di

    Twig::class => function ($container) // integration of slim/twig-view
    {
        return Twig::create('../src', []);
    },
    'view' => DI\get(Twig::class),//alias required by slim/twig-view component
    Client::class => function ($container)// integration of ElephantIO\Client
    {
        return new Client(new Version1X('http://127.0.0.1:3000'));
    },
]);

$container = $builder->build(); // integration of php-di

$app = Bridge::create($container);

$app->addMiddleware(TwigMiddleware::createFromContainer($app));// integration of slim\twig-view

$app->add(function ($request, $handler) {// solving CORS issues
    $response = $handler->handle($request);
    return $response
            ->withHeader('Access-Control-Allow-Origin', 'http://localhost:8000')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

$app->map(['GET', 'POST'], '/emit', function (
    Request $request, 
    Response $response, 
    MessageController $controller, 
    Twig $twig
) {
    return $controller->emit($request, $response, $twig);
});

$app->options('/emit', function ($request, $response) {// solving CORS issues
    return $response;
});

$app->run();
