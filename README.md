# Slim 4 Websocket

It's just a mvc boilerplate with integrated websocket server, that allows an easy extension and modification for various
needs and ideas.

Even if it might look a little bit scary or discouraging for hard core php developers considering nodejs engaged, 
there is no reason for that. It's just a spice, few lines of websocket server javascript code that you don't even have 
to think about, but because of expressjs presence, very easy for Slim 4 developers to experiment or modify eventually.

## Required

* PHP 7.0+

## File structure
* **slim-4-websocket**
    * **public**
        * index.php
    * **src**       
        * chat.html.twig
        * MessageController.php
        * MessageService.php
    * composer.json
    * package.json
    * server.js  *(websocket server - socket.io integration)*             

## Installation (on Ubuntu)

### 1. Add Node.js PPA

```sh
$ sudo apt-get update
$ sudo apt-get install curl
$ curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
```

### 2. Install Node.js and NPM
```sh
$ sudo apt-get install nodejs
```

### 3. Check Node.js and NPM Version
```sh
$ node -v
$ npm -v
```

### 4. Clone git repository
```sh
$ git clone https://paneric@bitbucket.org/paneric/slim-4-websocket.git
```

### 5. Install packages
```sh
$ cd slim-4-websocket
$ npm install
$ composer install
```

## Use

### 1. Start websocket server
```sh
$ npm run start
```
### 2. Start dev server
```sh
php -S localhost:8000 -t public
```
### 3. Browser
```sh
http://127.0.0.1:8000/emit
```
Open 2 or more tabs and start playing.

## Sources, inspirations and credits for

*  [socket.io: Nodejs websocket server](https://socket.io/)
*  [socket.io-client: Nodejs websocket client](https://socket.io/)
*  [elephant.io: PHP wrapper of socket.io-client](https://github.com/Wisembly/elephant.io)
*  [Sockets with PHP and Node: elephant.io example of integration](https://codeinphp.github.io/post/sockets-with-php-and-node/)
*  [Socket.io with Elephant.io: elephant.io example of integration](https://github.com/mairesweb/socket.io-elephant.io)
